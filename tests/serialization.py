# a low level internals test, for public API tests, see examples/api/

from warpseq.model.song import Song
from warpseq.model.device import Device
from warpseq.model.instrument import Instrument
from warpseq.model.note import Note
from warpseq.model.scale import Scale
from warpseq.model.scene import Scene
from warpseq.model.pattern import Pattern
from warpseq.model.track import Track
from warpseq.model.transform import Transform
from warpseq.model.clip import Clip
from warpseq.model.slot import Slot
from warpseq.api.exceptions import WebSlotCompilerException
from warpseq.model.data_pool import DataPool

import json
import traceback

def check_serialization(cls, obj):
    data1 = obj.to_dict()
    obj2 = cls.from_dict(song, data1)
    data2 = obj2.to_dict()
    assert (data1 == data2)

song = Song(name='foo')

device = Device(name='foo')
song.add_devices([device])

instrument = Instrument(name='foo', device=device, channel=1)
song.add_instruments([instrument])

root = Note(name='C', octave=1)

scale = Scale(name='foo', root=root, scale_type='major')
song.add_scales([scale])

scene = Scene(name='foo')
song.add_scenes([scene])

track = Track(name='foo', instrument=instrument)
song.add_tracks([track])

p1 = Pattern(name='foo', slots=[
    Slot(degree=4),
    Slot(degree=5),
    Slot(degree=6),
    Slot(degree=7)
])

web_slot_demo = [
    dict(degree="1-5"),
    dict(degree="1,2,3,4,5"),
    dict(degree="@foo", v_a='x', v_a_value="10", v_b='y', v_b_value='10-50', v_c='z', v_c_value='80,90,100', v_d='w', v_d_value="127"),
    dict(degree="", reset="0.5"),
    dict(degree="@foo,@bar"),
    dict(degree="@foo-@bar"),
    dict(degree=6, cc_a=10, cc_a_value=20, cc_b=40, cc_b_value="40-60", cc_c='37', cc_c_value=100, cc_d=50, cc_d_value="40,50,60"),
]

p2 = Pattern(name='bar', web_slots=web_slot_demo)

song.add_patterns([p1,p2])

t1 = Transform(name='foo', slots=[Slot(octave_shift=1),Slot(rest=True)])
t2 = Transform(name='bar', slots=[Slot(rest=True), Slot(octave_shift=1)])

song.add_transforms([t1,t2])

clip = Clip(name='foo', patterns=[p1,p2], transforms=[t1,t2], scales=[scale])
song.add_clip(scene=scene, track=track, clip=clip)


dp = DataPool(name='foo', web_slots=[dict(value=1), dict(value=2), dict(value=3)])


# check_serialization(Device, device)
# check_serialization(Instrument, instrument)
# check_serialization(Clip, clip)

from warpseq.parser.web_slots import *

#for category in [ CATEGORY_PITCH, CATEGORY_TIME, CATEGORY_MOD, CATEGORY_CONTROL ]:
#    print("--")
#    print(json.dumps(p2.get_webslot_grid_for_ui(category), indent=4, sort_keys=True))

p2.update_web_slots_for_ui(web_slot_demo)

data = p2.to_dict()

print(json.dumps(data, sort_keys=True, indent=4))


def test_web_slots(pattern, web_slots, expect_errors=0):

    print("-------------------------------------")
    print("TESTING web_slots: %s" % web_slots)
    print("... expecting error count: %s" % expect_errors)

    pattern.web_slots = web_slots
    try:
        pattern.set_slots_from_web_slots(web_slots)
        if expect_errors:
            raise Exception("did not cause an error")
    except WebSlotCompilerException as we:
        traceback.print_exc()
        if expect_errors == 0:
            raise Exception("FAILED")
        error_count = len(we.errors)
        assert error_count == expect_errors
    data = pattern.to_dict()
    print(json.dumps(data["slots"], sort_keys=True, indent=4))

# these tests are more meaningful if you look at (and understand) the output and are not really intended to be full automated
# tests at this point... just something to help me evaluate the webslots code.

test_web_slots(p2, [ dict(degree=1), ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, chord_type='major'), ], expect_errors=0)
#test_web_slots(p2, [ dict(degree=1, chord_type='fuzzy'), ], expect_errors=1)
test_web_slots(p2, [ dict(degree=1, chord_type='major', rest=0.5), ], expect_errors=0)

# reverse
test_web_slots(p2, [ dict(degree=1, chord_type='major', reverse=0.75), ], expect_errors=0)
#test_web_slots(p2, [ dict(degree=1, chord_type='major', reverse='dog'), ], expect_errors=1)
test_web_slots(p2, [ dict(degree=1, chord_type='major', reverse='@pool'), ], expect_errors=0)

# reset
test_web_slots(p2, [ dict(degree=1, chord_type='major', reset=0.5), ], expect_errors=0)

# tie
test_web_slots(p2, [ dict(tie=0.5), ], expect_errors=0)

# degree_shift w/ variants
test_web_slots(p2, [ dict(degree=1, degree_shift=2), ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, degree_shift='1-2'), ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, degree_shift='1,2,3,4'), ], expect_errors=0)

# octave_shift
test_web_slots(p2, [ dict(degree=1, octave_shift=2), ], expect_errors=0)
#test_web_slots(p2, [ dict(degree=1, octave_shift='dog'), ], expect_errors=1)


# variables w/ variations
test_web_slots(p2, [ dict(degree=1, v_a='x', v_a_value='10', v_b='y', v_b_value='10-100', v_c='z', v_c_value='10,100', v_d='w', v_d_value='dog'), ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, v_a='x', v_a_value='10', v_b='y', v_b_value='10-100', v_c='z', v_c_value='10,100', v_d='w', v_d_value='@pool'), ], expect_errors=0)

# track_grab
test_web_slots(p2, [ dict(track_grab='other_track') ], expect_errors=0)

# track_copy
# this is not  yet surfaced in webslots
# test_web_slots(p2, [ dict(track_copy='other_track') ], expect_errors=0)

# rest
test_web_slots(p2, [ dict(rest='true') ], expect_errors=0)

# ccs
test_web_slots(p2, [ dict(degree=1, cc_a='10', cc_a_value='100', cc_b='20', cc_b_value='10-100', cc_c='30', cc_c_value='10,100', cc_d='40', cc_d_value='20'), ], expect_errors=0)
#test_web_slots(p2, [ dict(degree=1, cc_a='10', cc_a_value='100', cc_b='30', cc_b_value='10-100', cc_c='dog', cc_c_value='10,100', cc_d='w', cc_d_value='@pool'), ], expect_errors=4)
#test_web_slots(p2, [ dict(degree=1, cc_a='10', cc_a_value='100', cc_b='30', cc_b_value='10-100', cc_c='@dog', cc_c_value='10,100', cc_d='w', cc_d_value='@pool'), ], expect_errors=4)
#test_web_slots(p2, [ dict(degree=1, cc_a='10', cc_a_value='100', cc_b='40', cc_b_value='$x', cc_c='50', cc_c_value='10,100', cc_d='w', cc_d_value='@pool'), ], expect_errors=4)

# delay
test_web_slots(p2, [ dict(degree=1, delay=0.5) ], expect_errors=0)

# velocity
test_web_slots(p2, [ dict(degree=1, velocity=100) ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, velocity=100) ], expect_errors=0)

# skip
test_web_slots(p2, [ dict(skip=5) ], expect_errors=0)

# shuffle
test_web_slots(p2, [ dict(shuffle='true') ], expect_errors=0)
test_web_slots(p2, [ dict(shuffle='no') ], expect_errors=0)
test_web_slots(p2, [ dict(rest='0.4') ], expect_errors=0)


# length
test_web_slots(p2, [ dict(note='C', octave=4, length=1.2) ], expect_errors=0)

# reverse
test_web_slots(p2, [ dict(reverse=0.4) ], expect_errors=0)
test_web_slots(p2, [ dict(reverse='true') ], expect_errors=0)

# sharp
test_web_slots(p2, [ dict(degree=1, sharp=0.4) ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, sharp=1) ], expect_errors=0)

# flat
test_web_slots(p2, [ dict(degree=1, flat=1) ], expect_errors=0)
test_web_slots(p2, [ dict(degree=1, flat='') ], expect_errors=0)

# inversion
test_web_slots(p2, [ dict(degree=1, inversion=2) ], expect_errors=0)


print("ALL TESTS PASSED")

print(dp.to_dict())