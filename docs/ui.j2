<h1>User Interface</h1>
<hr/>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Starting The Web Server") }}

<p>
The Warp UI is a local web application that works by talking to a process running on your local computer, the same computer
that is connected to all of your MIDI devices.
</p>

<p>
To start the program, from a source checkout:
</p>

{{ begin_code() }}
PYTHONPATH=. bin/warp
{{ end_code() }}

<p>
Then open your web browser to <a href="http://127.0.0.1:8000">http://127.0.0.1:8000</a>.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Basic Concepts") }}

<p>
The left side of the screen contains a list of types of objects. These are, top to bottom: <b>Song</b>, <b>Scenes</b>,
<b>Tracks</b>, <b>Devices</b>, <b>Instruments</b>, <b>Scales</b>, <b>Patterns</b>, <b>Transforms</b>, and <b>Data Pools</b>.
</p>

<p>
These are heavily described in the {{ doc('api', 'API Docs') }} but at a different level of depth.  For this guide, we'll explain
how you would use them but in a bit more conversational way, as you would use them when writing a new song.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Devices") }}

<p>
Click on "Devices" in the left most panel and see what MIDI devices show up to the right of that panel. Here, you should see
any virtual MIDI devices (such as an Apple IAC Bus or a Windows LoopMIDI Device), as well as any hardware devices you have
like a USB MIDI interface.  If you see no devices here, you won't be able to hear anything from Warp until you add or connect something.
Devices will be rediscovered when you restart the program.
</p>

<p>
Nothing about devices are editable, this is only here for informational purposes.
</p>


<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Instruments") }}

<p>
The next thing to do is define some instruments. In basic configurations, you may only have one instrument on each
track, but that's not all Warp can do.  Warp can send notes to multiple instruments from one track, even rotating notes
between different instruments. Warp can also spread chords out between multiple monosynths.
<p>

<p>
When defining a new instrument, it's required that you pick a <b>Device</b> and a MIDI <b>Channel</b>.  Here, you can
also set a <b>Base Octave</b> which will be added to the pitch of all notes when that instrument is used.
</p>

<p>
There are also some extra fields here like <b>Min Octave</b> and <b>Max Octave</b>, <b>Default Velocity</b> and <b>Muted</b>,
which are self explanatory.
</p>

<p>
There are also options for defining up to 8 pairs of notes/octaves for use in drum triggers. This is only used in percussive
patterns, but it saves remembering that a kick drum is note C3 or C4 and so on.
</p>

<p>
At this point, it's worth mentioning that octaves in Warp start at 0, not -1 or -2, as in some other programs.
Middle C is then a "C5".
<p>

<p>
Warp is an "AJAXY" program, so any edits are applied immediately. We'll talk about saving files a bit later.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Scales") }}

<p>
Warp songs can use any number of scales, but to use them, you must create them and give them names. A Scale has a
<b>Root</b> note and either a scale <b>Type</b> or a list of <b>Degrees</b>.

<p>
The built-in scale types right now are:
</p>

{{ begin_code(language='python') }}
   major              = [ 1, 2, 3, 4, 5, 6, 7 ],
   pentatonic         = [ 1, 2, 3, 5, 6 ],
   pentatonic_minor   = [ 1, 3, 4, 5, 7 ],
   natural_minor      = [ 1, 2, 'b3', 4, 5, 'b6', 'b7' ],
   blues              = [ 1, 'b3', 4, 'b5', 5, 'b7' ],
   dorian             = [ 1, 2, 'b3', 4, 5, 6, 'b7' ],
   chromatic          = [ 1, 'b2', 2, 'b3', 3, 4, 'b5', 5, 'b6', 6, 'b7', 7 ],
   harmonic_major     = [ 1, 2, 3, 4, 5, 'b6', 7 ],
   harmonic_minor     = [ 1, 2, 3, 4, 5, 'b6', 7 ],
   locrian            = [ 1, 'b2', 'b3', 4, 'b5', 'b6', 'b7' ],
   lydian             = [ 1, 2, 3, 'b5', 5, 6, 7 ],
   major_pentatonic   = [ 1, 2, 3, 5, 6 ],
   melodic_minor_asc  = [ 1, 2, 'b3', 4, 5, 'b7', 'b8', 8 ],
   melodic_minor_desc = [ 1, 2, 'b3', 4, 5, 'b6', 'b7', 8 ],
   minor_pentatonic   = [ 1, 'b3', 4, 5, 'b7' ],
   mixolydian         = [ 1, 2, 3, 4, 5, 6, 'b7' ],
   phrygian           = [ 1, 'b2', 'b3', 4, 5, 'b6', 'b7' ],
   japanese           = [ 1, 2, 4, 5, 6 ],
   akebono            = [ 1, 2, 'b3', 5, 6 ]
{{ end_code() }}

<p>
If you want to define your own scale, you can select from the <b>Degrees</b> multi-select to build your own. We will likely
expand the built-in list over time.
</p>

<p>
Now that scales are defined, you can then use them later by assigning them to a Pattern, Clip, or the Song (as a global
default).
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Patterns") }}

<p>
Patterns have the most involved editing experience in Warp.  First, we'll explain the top-level properties and then
we'll explain the clip grid.  This really makes the most sense by watching the video.
</p>

<p>
Rather than storing notes directly in a <b>Clip</b>, Warp stores sequences of notes in Patterns.  This allows multiple
clips to reuse patterns, or repeat patterns in a "A/B/A/B" or arbitrary sequences. Changing a Pattern in one place affects
anywhere the pattern is used.  This makes it easy to edit a repeating theme throughout a composition without editing
dozens of different clips, increasing potential for experimentation.
</p>

<p>
A pattern has an <b>Octave Shift</b> which is added to the base pitch of the <b>Instrument</b>. It also has as <b>Rate</b>.
If the Rate is 1, notes by default will be interpreted as 16th notes compared to the song tempo.  If the rate is 2, they will
be 32nd notes, and 0.5 means 8th notes, and 0.25 would be quarter notes. Any positive floating point value is allowed.
</p>

<p>
The pattern may assign a <b>Scale</b>.  If specified, this does not force notes to a scale, but rather references how
the scale degrees in the pattern are converted to actual notes. Warp does not use a piano roll, but rather patterns
are entered in terms of numeric scale degree.  By avoiding the "force to scale" mechanic, it is possible to access
accidentals outside the scale at any step.
</p>

<p>
The pattern direction determines the order in which the notes of the pattern are evaluated. Patterns can run in forward, reverse,
and a variety of random directions.  Some pattern data may also change the pattern direction.
</p>

<p>
The pattern may also specify a "drum config" which is a reference to an <b>Instrument</b> that has defined MIDI mappings
for percussion. If this is set and the <b>Pattern Type</b> is changed to <i>percussion</i>, the Pattern steps will
no longer show notes but will be tailored from drum entry.
</p>

<p>
Finally in terms of basic properties, the pattern can choose an <b>Instrument</b> to be auditioned with. The idea here is that patterns
are not specific to one instrument or track, but can be reused throughout the composition.  We make it easy to try
out a pattern with all of the instruments while editing it. You can experiment with multiple instruments or
patches on different tracks to decide what sounds best for your particular idea.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Patterns - Step Editor") }}

<p>
Below the pattern properties there is a grid where the pattern steps are to be entered.  The Grid has numerous columns so
 the columns, rather than being shown on one screen, are split into categories (<i>Common</i>, <i>Pitch</i>, <i>Time</i>,
 <i>Mod</i>, <i>Vars</i>, and <i>Ctrl</i>).  Fully complete songs can be made with just the <i>Common</i> tab, but
 it's useful to understand all of the fun options that are available.
</p>

<p>
Just to the left of the pseudo-tabs are is audition button that will play the current pattern with the selected <b>Audition</b>
instrument.  Warp allows for live editing, so it's fine to keep the loop playing as you edit it.
</p>

<p>
In each grid cell, you can generally type in a value, which is most often a number.  Chord types take a string.
</p>

<p>
Typing in a number like "0:4" defines a range - this means, "randomly select any number between 0 and 4"
</p>

<p>
Typing in a value like "0,0,3,4" means randomly pick one of the values from a list. Values are allowed to repeat.
</p>

<p>
Columns marked with an asterisk ("*") are boolean columns - they represent true or false values, except probabilities can be used.
A value of 0 will never be true, a value of 1 and higher is always true, and a value of 0.5 is true 50% of the time.
</p>

<p>
Most of these options also are used in <b>Transforms</b>, which are how we construct MIDI effects such as Arpeggiators.
Transforms will be explained in more detail after explaining the Pattern options.
</p>

{{ begin_warn_block() }}
At the current point in Warp development, if you enter in a "bad" value for a cell, like a scale degree of "narf!", the
system will assume a default value without visual feedback.  This is easy to avoid if you remember all cells except
one or two want numbers. Visual feedback will be added in the near future.  Inputing some things like a chord type
for a monophonic instrument are not the responsibility of Warp though, and are not errors per se - it's just that the
MIDI device on the other end doesn't know what to do with the input. That can also be true with some instruments
where notes repeat too quickly to have audible attack changes, or too quickly to be audible due to slow attacks. When in
doubt, you can always record the MIDI Warp is emitting in your DAW to see what it is producing.
{{ end_warn_block() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Common Options") }}

<p>
<b>Degree</b> - (integer) indicates the scale degree relative to the currently selected scale.  If no scale is set, a default
scale of C Major will be used.  Enter "1" for the tonic, or any value higher or lower. "-1" references a note one
lower than the tonic, and "0" will also get you the tonic. A value like "1,3,5" will randomly select the 1st, 3rd, or 5th
degree of the current scale, and a value of "1:4" will select a note in the first four scale degrees.
</p>

<p>
<b>Tie</b> - (boolean) to add a tie to lengthen a note, leave the <i>Degree</i> blank and fill in a "1" in the tie column one one row
after the note you want to tie.  Probabilities apply as with all boolean columns, so for a 40% chance of a tie, you can type in 0.4.
</p>

<p>
<b>Repeat</b> - (integer) if set, the note will repeat this many additional times within the space alloted, effectively stuttering
the note.  This will be more noticeable with slower tempos and can be used as an alternative form of accent. A value of "0:1" would
repeat the note once 50% of the time.
</p>
R
<p>
<b>Length</b> - (float) if set, multiplies the normal length of the note on this particular step.  0.5 would cut the note length
in half, and 2 would double it. Values larger than 1 may result in overlDapping notes depending on what is next in the pattern.
</p>

<p>
<b>Chord Type</b> - (string) if set, turns the note into a chord.  The valid chord types are listed below.  For randomly choosing
a chord type, seperate valid chord type names with a comma, like "major,minor,minor" for a 2/3 change of a minor chord and 1/3 chance
of a major chord.
</p>

<p>
<b>Octave +/i</b> - (integer) adds or subtracts from the current octave.  A value like "-2" would lower the octave by two.
A value like "-1,0,1" would either add, subtract, or not modify the current octave with equal probability.
</p>

<p>
<b>Velocity</b> - (integer) a numeric MIDI velocity value between 0 and 127.  Velocity can be easily humanized with range
expressions like "80:90" or choice expressions like "95,96,97,98". If not specified, a default velocity value is taken from
the <b>Instrument</b> configuration.
</b>
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Pitch Options") }}

Pitch includes some options from the Common tab and adds a few more:

<p>
<b>Sharp (#)</b> - (boolean) add a 1 here to turn the note sharp. For a 33% chance of making a note sharp, type "0.33".
This can (desirably) result in notes outside the current scale, depending on the chosen scale degree.
</p>

<p>
<b>Flat (b)</b> - (boolean) add a 1 here to turn the note flat.  If both sharp and flat are true, they will cancel out.
</p>

<p>
<b>Inversion</b> - (integer) if "1" or "2" or "3", selects the 1st or 2nd or 3rd inversion of a chord. To select from all inversions
use "0,1,2,3" or "0:3"
</p>

<p>
<b>Degree +/-</b> - (integer) this amount is added to the scale degree. It is most useful in transforms and can be negative.
</p>

<p>
<b>Octave +/-</b> - (integer) this amount is added to the chosen octave. It can be negative.
</p>


<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Time Options") }}

Time options correspond to note length and whether the note should fire at all.  Some options were already included
in "Common" options. Here are the additional ones.

<p>
<b>Rest</b> - (boolean) if 1, converts the note with certainty to a rest, silencing the note or chord.  This is immediately useful in transforms, though in a
regular pattern, is more interesting if a probabily like "0.4", indicating a chance the note will not play.
</p>

<p>
<b>Delay</b> - (float)


<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Mod Options") }}

<p>
<i>Velocity</i> was already shown in 'Common', additionally you will find pairs here
for configuring MIDI CCs.
</p>

</p>
The first value in each pair is the integer channel number.  These may not be randomized with ranges or choices (":" or ",").
After the CC, comes an integer value between 0 and 127. These may be randomized. For instance, "100:127" picks a value between
100 and 127.  MIDI CC values will be set until they are rewritten, and each step can assign up to three different MIDI CC
values to three different MIDI CC numbers.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Vars Options") }}

<p>
Here, much like under <i>Mod Options</i> the values come in pairs.  Except now, instead of MIDI CC controller messages,
we are setting string variable names and values.
</p>

<p>
Assigning a pair of values like "X" and "42" means that in the pattern grid (later on), if we type "$X" we will get
the value 42. The variables can also contain string values, like chord types.
</p>

<p>
Each slot can set up to  three variable values.  Variables are in a global namespace and can be used in other patterns as
well as in transforms.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Control Options") }}

<p>
Control options do things to the pattern that are a bit more aggressive than simply changing a pitch or length.
</p>

<p>
<B>Track grab<B> - (string). If a name of a track is written here, the note specified in <i>Degrees</i> is ignored and the note
value that is currently playing on the named track will be used instead.  If the track name is mispelled, the system will
not capture the playing note.  Randomization may be used, for instance "track1,track2" could choose to capture the note
value from "track1" or "track2" with an equal chance.
</p>

<p>
<B>Shuffle</B> - (boolean). If true, the pattern will shuffle randomly until reset or restarted.  This is best used with probability
values less than one, such as "0.1" for a 10% chance of reshuffling.
</p>

<p>
<B>Reverse</B> - (boolean). If true, this will reverse the pattern direction.  If the pattern direction was already set to a random
direction or "back and forth", this might not be noticeable.
</p>

<p>
<b>Reset</b> - (boolean). If true, the pattern will jump back to the start.
</p>

<p>
<b>Skip</b> - (integer).  If non-zero the system will skip the current note and possibly more additional notes.  This can be used
with probability effectively using patterns like "0,0,0,2" which would indicate a 1/4 chance of skipping ahead 2 notes.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Pattern & Transforms: Other Buttons") }}

We've now concluded explaining items in the Pattern and Transform editor grids, but haven't explained a few additional
navigation buttons above those grids.

<p>
<b>+1, +4, +8</b> - this adds a row (or 4 rows, or 8 rows) to the pattern.
</p>

<p>
<b>Del</b> - this deletes the selected rows from the pattern.  This cannot be undone.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Transforms") }}

<p>
Transforms are the mechanism that we build "MIDI Effects" in Warp.  These could include arpeggiators, simple octave shifts,
or expressions that turn simple notes into complex basslines. They are like patterns that modify patterns, and are very open ended.
In the near future we will add a guide or video describing basic arpeggiator and effect construction.
</p>

<p>
Each transform has a grid with parameters almost exactly like patterns do, and each step applies to each slot in a pattern in turn.
They don't have to be the same length.
</p>

<p>
Above the grid, the following parameters control how the pattern operates:
</p>

<p>
<B>Arpeggiate</B> - controls whether or not the transform is an arpeggiator, which means that it should break up chords
into notes played in order. If arpeggiate is off, notes in a chord will be kept together.
</p>

<p>
<B>Divide</B> - a positive integer value. If 1,  each step of the transform leaves each note as is. If greater than 1,
multiple steps of the transform will be applied to the note and played within the timespace that the note would have
originally occupied. As such, this may be thought of as a kind of speed, though it's also more than that.
</p>

<p>
<B>Applies To</B> - this can be "chords", "notes", or "both".  If "chords", individual notes will be played verbatim and not
affected by the transform.  This could be used to construct a chord strummer on monophonic synths that does not alter
normal notes.  If "notes", chords are kept intact, but notes are modified by the transform steps. "Both" affects, as expected,
both.
</p>

<p>
<b>Direction</b> - just as with patterns, transforms can run in forward, reverse, bi-directional, or various random orderings.
</p>

<p>
<b>Auto Reset</b> - a boolean value, that, if true, restarts the pattern at the beginning when the pattern using the transform
restarts.
</p>

<p>
Note that using the same transform in two simultaneously playing clips will cause each step of the transform to advance
for each note in the clip. You may wish to copy the transform, or this could be musically interesting.
</p>

<p>
Transforms do not have to be the same length of the patterns they modify, and in fact that can produce added musical
interestingness.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Data Pools") }}

<p>
Earlier, when discussing special values like "1:4" and "1,2,3,5" and "$X" that can be used in patterns and transforms, we
intentionally skipped over a fourth special way to enter in information.
<p>

<p>
Data pools are like patterns and can have <i>Directions</i>, but do not produce notes.  In any cell, to pull the next value from
a data pool (which may be thought of as a queue of sorts), assuming the data pool is named "d1",
you can type the name of the data pool like this: "@d1"
</p>

<p>
Data pools have a grid editor, where each value entered is verbatim value, and they can have any type (integer, floating
point, string, etc).
</p>

<p>
Data pools are especially useful for writing simple patterns that have a certain note of a step pull the scale degree
from a data pool, slowly evolving just a few selected notes in a musical phrase as the patterns loop around.  Data pools
are reset to the beginning of their cycle each time a scene advances if the property <i>Auto Reset</i> is set.
</p>


<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Song: Properties") }}

<p>
The song page contains the most important panel in Warp, the clip launcher.  However, before we get there the song has
some basic features that need to be mentioned.
</p>

<p>
<B>Tempo</B> - this sets the default tempo.  Any <i>Rate</i> set on a Clip or Pattern will be multiplied against this value.
</p>

<p>
<B>Filename</B> - this filename is used when saving (downloading) the song. Because Warp is a web application, saves
appear as file downloads in your Downloads folder.
</p>

<p>
<B>Scale</B> - this sets a default scale for the song if not overridden elsewhere, such as in a Scene or Clip.
</p>

<p>
To save the song, click <B>"Save Song"</B>, and similarly, to load a new song into warp, use <B>"Open Song"</B> to upload a file
from your computer.
</p>

<p>
<B>"Init Song"</B> will reset the song state to the empty default configuration, clearing any configurations, patterns, or settings
if not already saved.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Song: Editing Clips") }}

<p>
Warp works best when you build up a library of interesting patterns and transforms, and then, once happy with them, decide
to assemble them into a "Song", which is an arrangement of Clips that play together.  Clips contain a list of those patterns
and transforms you have previously created.
</p>

<p>
While we should have constructed and auditioned some patterns, we use the Clip Grid on the Song page to add clips.
Clicking any "+" sign will make a new clip for a given scene and track combination.
</p>

<p>
While on this page, we can also reorder Scenes by dragging them up and down, and can also choose to open any existing Scene, Track,
or Clip by clicking on their names.
</p>

<p>
Once we click on a "+" or an existing Clip Name, we are taken to the Clip Editor.  Clips have the following
properties:
</p>

<p>
<B>Rate</B> - a floating point rate that is multiplied against the scene tempo to determine how fast the notes should
play in this clip.  There is also a rate setting on the pattern, and both settings are used together.
</p>

<p><B>Cycle Limit</B> - this controls how many times the clip should play in a loop.  If this value is "0" the clip will
play infinitely until it is advanced by pressing play next to a new scene or track.  If 2, the clip would play two times.
</p>

<p><B>Advance Scene</B> - this boolean flag controls whether, when a given clip finishes, the entire scene will move on
to the next scene. This should usually be enabled for only one of many clips in a scene. For instance, a drum clip could
be set to loop infinitely (cycle limit = 0), and a musical pattern could be set to Advance the Scene after 4 repeats.
</p>

<p><B>Patterns (List)</B> - this widget provides a place to type names of patterns.  One cycle of a playing clip
will play all of these listed patterns in order. Pattern names can be repeated, such as to get a "A, B, A, B, C" type
repetition effect.
</p>

<p><B>Transforms (2D List)</B> - this widget provides a place to add a list of transforms. Each time a pattern advances to the next
pattern in the list, or a clip cycles around for another iteration, the next set of transforms from this list will be applied.
While it is possible to enter in only one transform in each slot, if multiple transforms are listed on the same horizontal row,
they will be applied in series, each modifiying the result of the past transform.  These lists do not need to have the same length
as the patterns list.</p>

<p><B>Scales (List)</B> - this widget provides a place to type the names of scales.  Each time a pattern advances or the clip cycles
around, a new scale will be chosen from this list. If this list is empty, the scale will be chosen from the pattern, if available,
and if not set there, the default scale for the song will be used.
</p>

<p><B>Tempo Shifts (List)</B> - this is a list of positive or negative integers that are added (or subtracted) to the effective tempo
of the song as the pattern plays.  For instance, entering in, on different lines, "0", "5", "10", "15" would effect a gradual speed
up as each pattern advances.  If more patterns play than entries in this list, the system will roll around and start back at the beginning.
If empty, no tempo shifts will be applied.  This is perhaps most useful around breaks or possibly song endings.</p>

<p>As with Patterns and Transforms, clips can be auditioned without leaving the Clips page.</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Song: Launching Clips") }}

Scenes full of clips, or just individual clips, can be launched by clicking the associated play buttons on the Song page.

{{ begin_info_block() }}
At this point, Warp does not wait until the end of a global bar length to launch the next clip. Any human-introduced
changes are immediate. Alternatives will be provided in the near future.
{{ end_info_block() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("What's Next?") }}

Experiment and have fun! Some more thorough explanations of these topics - though not entirely surfaced to the user
in the same way or syntax, are included in the API documentation. If you have questions or ideas, <A HREF="mailto:michael@michaeldehaan.net">
email Michael</A> any time.

