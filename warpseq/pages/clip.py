# ------------------------------------------------------------------
# Warp Sequencer
# (C) 2020 Michael DeHaan <michael@michaeldehaan.net>
# Apache2 Licensed
# ------------------------------------------------------------------

from warpseq.pages.base import BaseBuilder
from warpseq.server.widgets import textbox, select, multiple, button, rangebox, toggle, infobox
from warpseq.model.directions import DIRECTIONS


CUSTOM_JS = """
function intercept_field(field,value) {
   return true;
}
function edit_this() {
  edit_clip();
}

load_clip_pattern_grid();
load_clip_transform_grid();
load_clip_scale_grid();
load_clip_tempo_shift_grid();

"""

BONUS_HTML = """

<h2><font color='purple'><i class="fas fa-stroopwafel" aria-hidden="true"></i></font>&nbsp;&nbsp;&nbsp;Patterns</h2>

<div id="clip_pattern_grid" style="height: 350px; width:100%;" class="ag-theme-alpine"></div>

<h2><font color='purple'><i class="fas fa-frog" aria-hidden="true"></i></font>&nbsp;&nbsp;&nbsp;Transforms</h2>

<div id="clip_transform_grid" style="height: 350px; width:100%;" class="ag-theme-alpine"></div>

<h2><font color='blue'><i class="fas fa-music" aria-hidden="true"></i></font>&nbsp;&nbsp;&nbsp;Scales</h2>

<div id="clip_scale_grid" style="height: 350px; width:100%;" class="ag-theme-alpine"></div>

<h2><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;Tempo Shifts</h2>

<div id="clip_tempo_shift_grid" style="height: 350px; width:100%;" class="ag-theme-alpine"></div>

"""

SIDE_HTML = """
<font color='green'><i class="fas fa-paperclip fa-4x" aria-hidden="true"></i></font>
"""

class ClipBuilder(BaseBuilder):

    __slots__ = ()

    @classmethod
    def get_form_parameters(cls, data):
        return [
            textbox(data, "Clip", "new_name"),
            infobox(data, "Scene", "scene"),
            infobox(data, "Track", "track"),
            textbox(data, "Rate", "rate"),
            textbox(data, "Cycle Limit", "repeat"),
            toggle(data, "Advance Scene", "auto_scene_advance"),
        ]

    @classmethod
    def get_button2_parameters(self, data):
        return [
            button(caption="Audition", link_class='green_on_hover', fa_icon="fa-play", onclick="audition_clip()"),
            button(caption="Stop", link_class='red_on_hover', fa_icon="fa-stop", onclick="stop()"),
            # button(caption="1", fa_icon="fa-plus", id="new_row_button"),
            # button(caption="4", fa_icon="fa-plus", id="new_row4_button"),
            # button(caption="8", fa_icon="fa-plus", id="new_row16_button"),
            # button(caption="Del", fa_icon="fa-times", id="delete_row_button"),
        ]

    @classmethod
    def get_button_parameters(cls, data):
        return [
            button(caption="Delete", link_class="red_on_hover", danger=True, fa_class="far", fa_icon="fa-trash-alt"),
            button(caption="Close", link_class="blue_on_hover", fa_class="far", fa_icon="fa-window-close", onclick="load_song()"),
        ]

    @classmethod
    def get_custom_js(cls):
        return CUSTOM_JS

    @classmethod
    def has_grid(self):
        return False

    @classmethod
    def bonus_html(self):
        return BONUS_HTML

    @classmethod
    def side_html(self):
        return SIDE_HTML