<h1>Evaluators</h1>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
<h2>Evaluators</h2>

<p>
Evaluators are expressions that are valid for any value in a {{ doc('api_slots','Slot') }}. They can be used to achieve
controllable per-step randomness, pull values for a {{ doc('api_data_pools','Data Pools') }} or pull notes from one track into another.
</p>

<p>
Use of evaluators allows Warp to produce expressive, highly evolving compositions full of emergent behavior, operating off
a very small amount of data, and often zero custom programming. As the UI in Warp is built, evaluators will be fully
exposed to the user interface, and not remain exclusively a programming feature.
</p>

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ table_of_contents() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Probability") }}

In the example below, the 4th note has a 40% chance of being silenced:

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(degree=1),
    Slot(degree=2),
    Slot(degree=3),
    Slot(degree=4, rest=Probability(0.4,True,False)
])
{{ end_code() }}

In the example below, there is a 40% chance of an octave shift:

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(degree=1),
    Slot(degree=2),
    Slot(degree=3),
    Slot(degree=4, octave_shift=Probability(0.4,-2,0)
])
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("RandomChoice") }}

In the example below, the note is chosen from a limited set of possible notes:

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(degree=RandomChoice(1,2,4,7),
    Slot(degree=4),
    Slot(degree=5)
])
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("RandomRange") }}

In the example below, the velocity is selected within a narrow range, implementing humanization

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(degree=1, velocity=RandomRange(80,90),
    Slot(degree=2, velocity=RandomRange(80,95),
    Slot(degree=3, velocity=RandomRange(85,95),
])
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("LoadVariable") }}

<p>
Variables are global and can be shared between tracks. As a result, one pattern, perhaps even playing on a muted
track, can influence another.  Here is a trivial self-contained example:
</p>

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(degree=1, variables=dict(a=1, b=2)),
    Slot(degree=2),
    Slot(degree=LoadVariable('a')),
])
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("TrackGrab") }}

<p>
Track Grab listens to what note is playing on another track and replaces the current note with that
value. See this <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/12_note_hocketing.py">hocket example</a>.
</p>

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(track_grab='conductor')
])
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("DataGrab") }}

<p>
Somewhat like a <i>TrackGrab</i>, a Data Grab pulls the next value from a {{ doc('api_data_pools','Data Pool') }}.
This could be a fun way to interlace one melody into another. J.S. Bach, here we come!
</p>

{{ begin_code(language='python') }}
api.patterns.add(name='foo', slots=[
    Slot(degree=DataGrab('some_pool'),
    Slot(degree=4),
    Slot(degree=5),
    Slot(degree=DataGrab('some_pool'),
])
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Negate") }}

<p>
If you need to negate the value from a <i>DataGrab</i> you can't use the minus sign because a <i>DataGrab</i> is some
kind of magical <i>timey-whimey</i> object. You need to use <i>Negate</i>
</p>

{{ begin_code(language='python') }}
api.transforms.add(name='octave_shifter', slots=[
    Slot(octave_shift=1),
    Slot(octave_shift=Negate(DataGrab('mcelligots_pool')),
    Slot(octave_shift=1),
])
{{ end_code() }}

<p>
This is a preview of this next thing...
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Nesting Evaluators") }}

<p>
Technically you can nest Evaluators at infinite level. If you need to randomly decide to use a DataGrab or a constant,
for example:
</p>

{{ begin_code(language='python') }}
api.transforms.add(name='maximum_chaos', slots=[
    Slot(octave_shift=1),
    Slot(octave_shift=Probability(0.5, DataGrab('the_deep_end'), 2),
    Slot(octave_shift=1),
])
{{ end_code() }}

<p>
Any parameter to any evaluator can be subject to nesting.
</p>

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ section("Suggestions & Tips") }}

<ul>
<li>Per-step controllable randomness is often more interesting than lots of randomness throughout a track</li>
<li>Using <i>DataGrab</i> may sometimes be more interesting than randomness</li>
</ul>


<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("In Context") }}

Reading the following API examples is recommended for understanding this section:

<ul>
<li><A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/06_intra_track_events.py">Intra Track Events</A>
<li><A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/07_variables.py">Variables</A>
<li><A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/09_data_pools.py">Data Pools</A>
<li><A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/15_pattern_direction_change2.py">Pattern Direction Change 2</A></li>
<li><A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/17_note_skips.py">Note Skips</A>
</ul>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Next Steps") }}

This is already pretty far into the Warp feature set.  Be sure to read about {{ doc('api_data_pools') }} which are used with
<i>DataGrab</i>.


